import SensorCode
from matplotlib import pyplot as plt


if __name__ == "__main__":
    zipfiles = []
    zipfiles.append("../olaf/Jubi_ContactSp_3bar_Neffos_rechts/accel_and_location 2023-01-24 16-46-20.zip")
    zipfiles.append("../olaf/Jubi_ContactSp_4bar_Neffos_rechts/accel_and_location 2023-01-24 16-34-17.zip")
    zipfiles.append("../olaf/Terra_ContiSpike_3bar_Neffos_rechts/accel_and_location 2023-01-22 12-53-06.zip")
    zipfiles.append("../olaf/Terra_ContiSpike_4bar_Neffos_links/accel_and_location 2023-01-22 12-33-49.zip")
    zipfiles.append("../olaf/Terra_ContiSpike_4bar_Neffos_rechts/accel_and_location 2023-01-22 12-27-47.zip")
    zipfiles.append("../olaf/Terra_ContiSpike_6bar_G180_rechts/My Experiment 2023-01-22 12-45-45.zip")
    zipfiles.append("../olaf/Terra_ContiSpike_6bar_Neffos_rechts/accel_and_location 2023-01-22 12-39-33.zip")
    zipfiles.append("whg-forschungszentrum.zip")
    zipfiles.append("../Arbeitsweg-Heim/ForschungsZentrumAngermair.zip")

    outfile = open("../batch-output/batch.html", "w")
    outfile.write("<html>\n<head>\n</head>\n<body>\n")

    outfile.write("<h1>Radwegeanalyse Batch</h1>")

    for zipfile in zipfiles:
        title = ""
        if len(zipfile.split("/")) >= 2:
            title = zipfile.split("/")[2]
        else:
            title = zipfile.replace("/", "_")
        print("Dealing with ", title)
        outfile.write("<h2>" + title + "</h2>\n")

        # Reading the zipfile
        geoPoints = SensorCode.readGeo(zipfile, "Location.csv")
        accPoints = SensorCode.readAcc(zipfile, "Accelerometer.csv")

        # Basic data
        outfile.write("<ul>\n<li>Anzahl GPS Points: " + str(len(geoPoints))  +  "</li>\n")
        outfile.write("<li>Anzahl Bewegungs Points: " + str(len(accPoints)) + "</li>\n</ul>\n")

        # Plotting first histogram of acc data
        histogram = SensorCode.drawHistogram(accPoints)
        png_file_name = title +  "-01.png"
        plt.savefig('../batch-output/' + png_file_name)
        outfile.write("<h3>Histogram der Z-Werte</h3>\n")
        outfile.write("<p>\n<img src='" + png_file_name + "'></p>\n")

        # Calculating window and assigning acc value to geo point
        geoPoints = SensorCode.calculateAccValues(geoPoints, accPoints, "maximum")

        histogram = SensorCode.drawHistogram(geoPoints)
        png_file_name = title +  "-02.png"
        plt.savefig('../batch-output/' + png_file_name)
        outfile.write("<h3>Maximaler Wert pro Fenster dem Geo Point zugewiesen</h3>\n")        
        outfile.write("<p>\n<img src='" + png_file_name + "'></p>\n")

        # Map of the maximum points
        
        outfile.write("<h3>Karte der maximalen Werte</h3>\n")
        lat, lon = SensorCode.calcGeoMiddle(geoPoints)
        treshhold = 15
        while treshhold < 35:
            m = SensorCode.drawMap(geoPoints, lat, lon, treshhold)
            html_file_name = title +  "_" + str(treshhold) + ".html"
            m.save('../batch-output/' + html_file_name)        
            outfile.write("<p><a href='" + html_file_name + "'>[Karte Treshhold " + str(treshhold) + "]</a></p>\n")
            treshhold = treshhold + 5

        # Export geo points to csv
        csv_file_name = title +  ".csv"
        csvfile = open("../batch-output/" + csv_file_name, "w")
        csvfile.write("lat\tlon\tvalue\n")
        for p in geoPoints:
            csvfile.write(str(p.latitude) + "\t" + str(p.longitude) + "\t" + str(p.accValue) + "\n")




        csvfile.close()


    outfile.write("</body>\n</html>")
    outfile.close()





