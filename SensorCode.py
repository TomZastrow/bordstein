import zipfile
import folium
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np

class geoPoint:
    time = 0.0
    latitude = 0.0
    longitude = 0.0
    height = 0.0
    velocity = 0.0
    direction = 0.0
    horacc = 0.0
    vertacc = 0.0
    accValue = 0.0
    popup = "Text"

class accPoint:
    time = 0.0
    x = 0.0
    y = 0.0
    z = 0.0

def calcGeoMiddle(points):
    lat = 0
    lon = 0
    for point in points:
        lat = lat + point.latitude
        lon = lon + point.longitude

    lat = lat / len(points)
    lon = lon / len(points)

    return lat, lon

def readAcc(archive, theFile):
    points = []
    sensordata = zipfile.ZipFile(archive, "r")
    bytes = sensordata.read(theFile)
    lines = bytes.decode("utf-8").split("\n")

    lines.pop(0)
    for line in lines:
        rec = line.strip().split("\t")
        if len(rec) == 4:
            aPoint = accPoint()
            aPoint.time = float(rec[0])
            aPoint.x = float(rec[1])
            aPoint.y = float(rec[2])
            aPoint.z = float(rec[3])
            points.append(aPoint)

    return points


def readGeo(archive, theFile):
    points = []

    sensordata = zipfile.ZipFile(archive, "r")
    bytes = sensordata.read(theFile)
    lines = bytes.decode("utf-8").split("\n")

    lines.pop(0)
    for line in lines:
        rec = line.strip().split("\t")
        if len(rec) == 8:
            aPoint = geoPoint()
            aPoint.time = float(rec[0])
            aPoint.latitude = float(rec[1])
            aPoint.longitude = float(rec[2])
            aPoint.height = float(rec[3])
            aPoint.velocity = float(rec[4])
            aPoint.direction = float(rec[5])
            aPoint.horacc = float(rec[6])
            aPoint.vertacc = float(rec[7])
            points.append(aPoint)

    return points

def calculateAccValues(geoPoints, accPoints, accType):
    window = len(accPoints) // len(geoPoints)
    print("Window size: ", window)

    start = 0

    min = 0
    max = 0
    minD = 0
    maxD = 0

    for j in range(len(geoPoints)):
        ende = start + window
        summe = 0.0
        accMax = 0.0
        for i in range(start, ende):
            if abs(accPoints[i].z) > accMax:
                accMax = abs(accPoints[i].z)
            summe = summe + abs(accPoints[i].z)
            if accPoints[i].z < min:
                min = accPoints[i].z
            if accPoints[i].z > max:
                max = accPoints[i].z

        if accType == "average":        
            durchschnitt = summe / window
            geoPoints[j].accValue = durchschnitt
            if durchschnitt < minD:
                minD = durchschnitt
            if durchschnitt > maxD:
                maxD = durchschnitt
        else:
            geoPoints[j].accValue = accMax

        

        start = ende

    print("Minimum der Z-Daten: ", min)
    print("Maximum der Z-Daten: ", max)
    print("Minimum der interpolierten Z-Daten: ", minD)
    print("Maximum der interpolierten Z-Daten: ", maxD)
    return geoPoints

def drawHistogram(accPoints):
    zData = []
    tData = []
    for p in accPoints:
        zData.append(p.time)
        if isinstance(p, accPoint):
            tData.append(p.z)
        if isinstance(p, geoPoint):
            tData.append(p.accValue)
        
    fig, ax = plt.subplots()
    return ax.plot(zData,tData)

def drawMap(geoPoints, lat, lon, threshold):
    m = folium.Map(location=[lat,lon], zoom_start=15)

    for point in geoPoints:
        theColor = "grey"
        theRadius = 2

        if point.accValue >= threshold:
            theColor = "red"
            theRadius = 10

        folium.Circle(
            radius=theRadius,
            location=[point.latitude, point.longitude],
            popup=str(point.accValue),
            fill_opacity=1,
            line_opacity=0,
            fill_color=theColor,
            color=theColor,
            fill=True,

        ).add_to(m)

    return m

def biggestPoints(accPoints, geoPoints, threshhold):
    biggest = []

    for point in accPoints:
        if point.z > threshhold:
        
            for i in range(1, len(geoPoints)-1):
                if geoPoints[i-1].time < point.time and geoPoints[i+1].time > point.time:
                    geoPoints[i].accValue = point.z
                    biggest.append(geoPoints[i])
                    break

    return biggest

def saveGeoPoints(points, filename):
    outfile = open(filename, "w")

    outfile.write("time\taccValue\tlatitude\tlongitude\theight\tvelocity\tdirection\thoracc\tvertacc\n")
    for p in points:
       outfile.write(str(p.time) + "\t" + str(p.accValue) + "\t" + str(p.latitude) + "\t" + str(p.longitude) + "\t" + str(p.height) + "\t" + str(p.velocity) + "\t" + str(p.direction) + "\t" + str(p.horacc) + "\t" + str(p.vertacc) +"\n")

    outfile.close()

